# Kadi4Mat

**Kadi4Mat** is the **Karlsruhe Data Infrastructure for Materials Science**, a
software for managing research data with the aim of combining new concepts with
established technologies and existing solutions.

## Links

* More information: https://kadi.iam-cms.kit.edu
* Source code: https://gitlab.com/iam-cms/kadi
* Releases: https://pypi.org/project/kadi
* Developer documentation:
    * Latest (reflecting the *master* branch): https://kadi4mat.readthedocs.io/en/latest
    * Stable (reflecting the latest release): https://kadi4mat.readthedocs.io/en/stable
