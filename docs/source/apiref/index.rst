API reference
=============

This chapter serves as an API reference for the source code of Kadi4Mat, as
well as the project's general structure.

.. toctree::
   :maxdepth: 1

   Project structure <project_structure>
   Models <models>
   Lib <lib>
   Modules <modules>
   Plugins <plugins>
