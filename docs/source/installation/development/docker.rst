.. _installation-development-docker:

Installation via Docker
=======================

The installation via Docker is preferable when one wants to quickly get up and
running with a working development environment. It is still recommended to
check out the :ref:`manual installation <installation-development-manual>`
anyway, as some aspects are explained there in more detail.

Installing the dependencies
---------------------------

The only dependency that is needed for this type of installation is `Docker
<https://www.docker.com/>`__, specifically the Docker Engine as well as Docker
Compose for running multi-container Docker applications.

The former can be installed running the following commands:

.. code-block:: bash

  sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
  curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
  sudo apt update && sudo apt install docker-ce docker-ce-cli containerd.io

The latest Docker Compose binary has to be installed separately, which can be
done like this:

.. code-block:: bash

  sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose

If Docker Compose is not usable inside a terminal via ``docker-compose``
afterwards, make sure that the ``/usr/local/bin`` directory is in the ``PATH``.

Running the application
-----------------------

To run the application as well as all necessary services, the following script
included in the Kadi4Mat source code can be used, which will first build all
services and then start them in the background:

.. code-block:: bash

  ${HOME}/workspace/kadi/bin/run_docker.sh

There are various ways to manage and interact with the services, i.e. the
running Docker containers, after starting them. The most common commands using
``docker-compose`` are explained below. For more detailed instructions it is
recommended to check out the official documentations of `Docker
<https://docs.docker.com/>`__ and `Docker Compose
<https://docs.docker.com/compose/>`__. Note that the ``docker-compose`` command
needs access to the ``docker-compose.yml`` file in the project's root
directory, so it is best to run it directly inside that directory.

To attach a terminal to the live log output of all services, including
timestamps, the following command can be used:

.. code-block:: bash

  docker-compose logs -tf

To stop and start the all services, the following commands can be used:

.. code-block:: bash

  docker-compose stop
  docker-compose start

To run arbitrary commands inside a container, the following command can be
used:

.. code-block:: bash

  docker-compose exec <service> <command>

For example, the following commands, which are run in the context of the kadi
service, are especially useful for development:

.. code-block:: bash

  docker-compose exec kadi kadi db test-data --i-am-sure                        # Setup some test data
  docker-compose exec kadi kadi celery worker -B --loglevel=INFO                # Start Celery to run background tasks
  docker-compose exec kadi python -m smtpd -n -c DebuggingServer localhost:8025 # Start a local debugging SMTP server

For more information about these commands, please refer to the :ref:`manual
installation <installation-development-manual>`.

Updating the application
------------------------

After updating the application code, it should be enough to restart all
services using:

.. code-block:: bash

  docker-compose restart
