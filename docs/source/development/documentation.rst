Documentation
=============

This section describes different aspects about the application's documentation.
It is assumed that either a manual or a hybrid development installation is
being used.

Documenting source code
-----------------------

Generally, all code should be documented using docstrings in a format suitable
for extraction. Please refer to the :ref:`Style guide
<development-style_guide>` for information about using docstrings in different
languages.

Building the documentation
--------------------------

The source files of the documentation can be found in ``docs/source``.
Generating HTML or other output from those files requires `Sphinx
<https://www.sphinx-doc.org/en/master/index.html>`__, which can be installed
using the extra ``[docs]`` dependencies defined in :file:`setup.py`:

.. code-block:: bash

  pip install -e ${HOME}/workspace/kadi[docs]

Afterwards, the documentation can be built using:

.. code-block:: bash

  make -C docs/ html

The generated documentation can then be found inside the ``build/html``
directory:

.. code-block:: bash

  firefox docs/build/html/index.html
