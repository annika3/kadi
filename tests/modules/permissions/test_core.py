# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.web import make_next_url
from kadi.lib.web import url_for
from kadi.modules.accounts.models import User
from kadi.modules.permissions.core import get_permitted_objects
from kadi.modules.permissions.core import has_permission
from kadi.modules.permissions.models import Permission
from kadi.modules.permissions.models import Role
from kadi.modules.permissions.utils import add_role
from tests.utils import check_api_response
from tests.utils import check_view_response


def test_has_permission(dummy_record, dummy_user):
    """Test if permissions are determined correctly."""
    action = "read"
    object_name = "record"
    object_id = dummy_record.id

    assert not has_permission(dummy_user, "test", object_name, object_id)
    assert not has_permission(dummy_user, action, "test", object_id)
    assert not has_permission(dummy_user, action, object_name, None)
    assert not has_permission(dummy_user, action, object_name, dummy_record.id + 1)
    assert has_permission(dummy_user, action, object_name, object_id)


def test_get_permitted_objects(dummy_record, dummy_user):
    """Test if permitted objects are determined correctly."""
    action = "read"
    object_name = "record"

    assert get_permitted_objects(dummy_user, action, "test") is None
    assert not get_permitted_objects(dummy_user, "test", object_name).all()

    permitted_objects = get_permitted_objects(dummy_user, action, object_name)

    assert permitted_objects.count() == 1
    assert permitted_objects.first().id == dummy_record.id


def test_permission_required_api(
    api_client, dummy_access_token, dummy_record, new_access_token, new_user
):
    """Test the "permission_required" decorator for API requests."""
    user = new_user()
    token = new_access_token(user=user)

    # Unauthorized request.
    response = api_client().get(url_for("api.get_record", id=dummy_record.id))
    check_api_response(response, status_code=401)

    # Unpermitted request.
    response = api_client(token).get(url_for("api.get_record", id=dummy_record.id))
    check_api_response(response, status_code=403)

    # Permitted request.
    add_role(user, "record", dummy_record.id, "member")

    response = api_client(token).get(url_for("api.get_record", id=dummy_record.id))
    check_api_response(response)

    # Invalid object ID.
    response = api_client(token).get(url_for("api.get_record", id=dummy_record.id + 1))
    check_api_response(response, status_code=404)


def test_permission_required_views(client, dummy_record, new_user, user_session):
    """Test the "permission_required" decorator for non-API requests."""
    username = password = "test"
    user = new_user(username=username, password=password)

    # Unauthorized request.
    endpoint = url_for("records.view_record", id=dummy_record.id)
    response = client.get(endpoint)

    check_view_response(response, status_code=302)
    assert response.location == make_next_url(endpoint)

    with user_session(username=username, password=password):
        # Unpermitted request.
        response = client.get(url_for("records.view_record", id=dummy_record.id))
        check_view_response(response, status_code=403)

        # Permitted request.
        add_role(user, "record", dummy_record.id, "member")

        response = client.get(url_for("records.view_record", id=dummy_record.id))
        check_view_response(response)

        # Invalid object ID.
        response = client.get(url_for("records.view_record", id=dummy_record.id + 1))
        check_view_response(response, status_code=404)


def _check_permissions(
    subject, valid_actions, invalid_actions, resource, check_global_permission=False
):
    object_name = resource.__class__.__tablename__

    for action in valid_actions:
        assert has_permission(subject, action, object_name, resource.id)

        if check_global_permission:
            assert has_permission(subject, action, object_name, None)

        permitted_objects = get_permitted_objects(subject, action, object_name)

        # For the special case when checking global permissions and the subject is a
        # group, which will itself be included in the permitted objects.
        if subject in permitted_objects:
            assert permitted_objects.count() == 2
            assert resource in permitted_objects
        else:
            assert permitted_objects.one() == resource

    for action in invalid_actions:
        assert not has_permission(subject, action, object_name, resource.id)
        assert not has_permission(subject, action, object_name, None)

        assert get_permitted_objects(subject, action, object_name)


SUBJECT_FACTORY_FIXTURES = ["new_user", "new_group"]


DUMMY_RESOURCES_FIXTURES = [
    "dummy_record",
    "dummy_collection",
    "dummy_group",
    "dummy_template",
]


@pytest.mark.parametrize("subject_factory_fixture", SUBJECT_FACTORY_FIXTURES)
@pytest.mark.parametrize("dummy_resource_fixture", DUMMY_RESOURCES_FIXTURES)
def test_permissions(
    subject_factory_fixture, dummy_resource_fixture, dummy_group, getfixture
):
    """Test if permissions work correctly for different resources."""
    subject = getfixture(subject_factory_fixture)()
    resource = getfixture(dummy_resource_fixture)
    model = resource.__class__
    object_name = model.__tablename__

    all_actions = [action[0] for action in model.Meta.permissions["actions"]]
    valid_actions = [all_actions[0]]
    invalid_actions = [action for action in all_actions if action not in valid_actions]

    permission = Permission.query.filter_by(
        action=all_actions[0], object=object_name, object_id=resource.id
    ).first()
    subject.permissions.append(permission)

    _check_permissions(subject, valid_actions, invalid_actions, resource)

    # In case of a user, test the group permissions as well.
    if isinstance(subject, User):
        subject.permissions = []
        dummy_group.permissions.append(permission)
        add_role(subject, "group", dummy_group.id, "member")

        _check_permissions(subject, valid_actions, invalid_actions, resource)


@pytest.mark.parametrize("subject_factory_fixture", SUBJECT_FACTORY_FIXTURES)
@pytest.mark.parametrize("dummy_resource_fixture", DUMMY_RESOURCES_FIXTURES)
def test_global_permissions(
    subject_factory_fixture, dummy_resource_fixture, dummy_group, getfixture
):
    """Test if global permissions work correctly for different resources."""
    subject = getfixture(subject_factory_fixture)()
    resource = getfixture(dummy_resource_fixture)
    model = resource.__class__
    object_name = model.__tablename__

    all_actions = [action[0] for action in model.Meta.permissions["actions"]]
    valid_actions = [all_actions[0]]
    invalid_actions = [action for action in all_actions if action not in valid_actions]

    permission = Permission.query.filter_by(
        action=all_actions[0], object=object_name, object_id=None
    ).first()
    subject.permissions.append(permission)

    _check_permissions(
        subject, valid_actions, invalid_actions, resource, check_global_permission=True
    )

    # In case of a user, test the group permissions as well.
    if isinstance(subject, User):
        subject.permissions = []
        dummy_group.permissions.append(permission)
        add_role(subject, "group", dummy_group.id, "member")

        _check_permissions(
            subject,
            valid_actions,
            invalid_actions,
            resource,
            check_global_permission=True,
        )


@pytest.mark.parametrize("subject_factory_fixture", SUBJECT_FACTORY_FIXTURES)
@pytest.mark.parametrize("dummy_resource_fixture", DUMMY_RESOURCES_FIXTURES)
def test_default_permissions(
    subject_factory_fixture, dummy_resource_fixture, getfixture
):
    """Test if default permissions work correctly for different resources."""
    subject = getfixture(subject_factory_fixture)()
    resource = getfixture(dummy_resource_fixture)
    model = resource.__class__

    all_actions = [action[0] for action in model.Meta.permissions["actions"]]

    for action, attrs in model.Meta.permissions.get("default_permissions", {}).items():
        valid_actions = [action]
        invalid_actions = [
            action for action in all_actions if action not in valid_actions
        ]

        for attr, value in attrs.items():
            prev_value = value
            setattr(resource, attr, value)

            # Unfortunately we have to assume that no other attributes grant the same
            # default permission.
            _check_permissions(subject, valid_actions, invalid_actions, resource)

            setattr(resource, attr, prev_value)


@pytest.mark.parametrize("subject_factory_fixture", SUBJECT_FACTORY_FIXTURES)
@pytest.mark.parametrize("dummy_resource_fixture", DUMMY_RESOURCES_FIXTURES)
def test_roles(
    subject_factory_fixture, dummy_resource_fixture, dummy_group, getfixture
):
    """Test if roles work correctly for different resources."""
    subject = getfixture(subject_factory_fixture)()
    resource = getfixture(dummy_resource_fixture)
    model = resource.__class__
    object_name = model.__tablename__

    all_actions = [action[0] for action in model.Meta.permissions["actions"]]
    valid_actions = model.Meta.permissions["roles"][0][1]
    invalid_actions = [action for action in all_actions if action not in valid_actions]

    role = Role.query.filter_by(
        name=model.Meta.permissions["roles"][0][0],
        object=object_name,
        object_id=resource.id,
    ).first()
    subject.roles.append(role)

    _check_permissions(subject, valid_actions, invalid_actions, resource)

    # In case of a user, test the group permissions as well.
    if isinstance(subject, User):
        subject.roles = []
        dummy_group.roles.append(role)
        add_role(subject, "group", dummy_group.id, "member")

        _check_permissions(subject, valid_actions, invalid_actions, resource)
