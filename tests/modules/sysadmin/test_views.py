# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from kadi.modules.accounts.models import LocalIdentity
from tests.utils import check_view_response


def test_information(client, dummy_user, user_session):
    """Test the "sysadmin.information" endpoint."""
    dummy_user.is_sysadmin = True

    with user_session():
        response = client.get(url_for("sysadmin.information"))
        check_view_response(response)


def test_manage_users(client, dummy_user, user_session):
    """Test the "sysadmin.manage_users" endpoint."""
    dummy_user.is_sysadmin = True
    new_username = "test"

    with user_session():
        response = client.get(url_for("sysadmin.manage_users"))
        check_view_response(response)

        response = client.post(
            url_for("sysadmin.manage_users"),
            data={
                "username": new_username,
                "displayname": new_username,
                "email": "test@test.com",
            },
        )

        check_view_response(response)
        assert LocalIdentity.query.filter_by(username=new_username).first()
