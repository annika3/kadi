# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os

from flask import current_app

from kadi.lib.storage.misc import create_misc_uploads_path
from kadi.modules.groups.utils import delete_group_image
from kadi.modules.groups.utils import get_user_groups
from kadi.modules.groups.utils import save_group_image
from kadi.modules.permissions.utils import add_role


def test_get_user_groups(dummy_group, dummy_user, new_user):
    """Test if determining user group membership works correctly."""
    groups = get_user_groups(dummy_user).all()

    assert len(groups) == 1
    assert groups[0].id == dummy_group.id

    user = new_user()
    add_role(user, "group", dummy_group.id, "member")
    groups = get_user_groups(user).all()

    assert len(groups) == 1
    assert groups[0].id == dummy_group.id


def test_save_group_image(monkeypatch, tmp_path, dummy_image, dummy_group):
    """Test if saving a group image works correctly."""
    monkeypatch.setitem(current_app.config, "MISC_UPLOADS_PATH", tmp_path)

    save_group_image(dummy_group, dummy_image)
    identifier = str(dummy_group.image_name)

    assert os.listdir(tmp_path)[0] == identifier[:2]
    assert os.path.isfile(create_misc_uploads_path(identifier))


def test_delete_group_image(monkeypatch, tmp_path, dummy_image, dummy_group):
    """Test if deleting a group image works correctly."""
    monkeypatch.setitem(current_app.config, "MISC_UPLOADS_PATH", tmp_path)

    save_group_image(dummy_group, dummy_image)
    delete_group_image(dummy_group)

    assert dummy_group.image_name is None
    assert not os.listdir(tmp_path)
