# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.modules.groups.core import create_group
from kadi.modules.groups.core import delete_group
from kadi.modules.groups.core import purge_group
from kadi.modules.groups.core import restore_group
from kadi.modules.groups.core import update_group
from kadi.modules.groups.models import Group
from kadi.modules.permissions.utils import add_role


def test_create_group(dummy_user):
    """Test if groups are created correctly."""
    group = create_group(
        creator=dummy_user,
        identifier="test",
        title="test",
        description="# test",
        visibility="private",
    )

    assert Group.query.filter_by(identifier="test").first().id == group.id
    assert group.plain_description == "test"
    assert group.revisions.count() == 1


def test_update_group(db, dummy_group, user_context):
    """Test if groups are updated correctly."""
    with user_context():
        update_group(dummy_group, description="# test")

        assert dummy_group.plain_description == "test"
        assert dummy_group.revisions.count() == 2


def test_delete_group(dummy_group, user_context):
    """Test if groups are deleted correctly."""
    with user_context():
        delete_group(dummy_group)

        assert dummy_group.state == "deleted"
        assert dummy_group.revisions.count() == 2


def test_restore_group(dummy_group, user_context):
    """Test if groups are restored correctly."""
    with user_context():
        delete_group(dummy_group)
        restore_group(dummy_group)

        assert dummy_group.state == "active"
        assert dummy_group.revisions.count() == 3


def test_purge_group(db, dummy_group, new_user, user_context):
    """Test if groups are purged correctly."""
    with user_context():
        user = new_user()

        add_role(user, "group", dummy_group.id, "member")

        purge_group(dummy_group)
        db.session.commit()

        assert Group.query.get(dummy_group.id) is None
        # Only the system role should remain.
        assert user.roles.count() == 1
