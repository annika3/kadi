# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.licenses.models import License
from kadi.modules.templates.forms import NewRecordTemplateForm


def test_new_template_form(db, dummy_template, dummy_record):
    """Test if prefilling a "NewRecordTemplateForm" with a template works correctly."""
    license = License.create(name="name", title="title")
    db.session.commit()

    dummy_template.data = {"type": "test", "license": "name", "tags": ["test"]}

    type = dummy_template.data["type"]
    tag = dummy_template.data["tags"][0]

    form = NewRecordTemplateForm(template=dummy_template)

    assert form.record_type.initial == (type, type)
    assert form.record_license.initial == (license.name, license.title)
    assert form.record_tags.initial == [(tag, tag)]
