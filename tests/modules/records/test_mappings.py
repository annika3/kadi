# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.modules.records.mappings import RecordMapping


def test_record_mapping(dummy_record):
    """Test if the mapping for records works correctly."""
    dummy_record.extras = [
        {"key": "test", "type": "str", "value": None},
        {
            "key": "test2",
            "type": "dict",
            "value": [{"key": "test", "type": "str", "value": "test"}],
        },
        {"key": "test3", "type": "list", "value": [{"type": "str", "value": "test"}]},
    ]

    doc = RecordMapping.create_document(dummy_record)

    assert doc.meta.id == dummy_record.id
    assert doc.identifier == dummy_record.identifier
    assert doc.extras_str == [
        {"key": "test", "type": "str", "value": None},
        {"key": "test2.test", "type": "str", "value": "test"},
        {"key": "test3.1", "type": "str", "value": "test"},
    ]
