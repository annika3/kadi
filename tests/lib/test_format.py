# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from datetime import datetime
from datetime import timezone

import kadi.lib.constants as const
from kadi.lib.format import durationformat
from kadi.lib.format import pretty_type_name
from kadi.lib.format import timestamp
from kadi.modules.records.models import Record


def test_durationformat():
    """Test if durations are formatted correctly."""
    assert durationformat(0) == "0 seconds"
    assert durationformat(1) == "1 second"
    assert durationformat(const.ONE_MINUTE) == "1 minute"
    assert durationformat(const.ONE_HOUR) == "1 hour"
    assert durationformat(const.ONE_DAY) == "1 day"
    assert durationformat(const.ONE_WEEK) == "1 week"
    assert (
        durationformat(
            const.ONE_WEEK + const.ONE_DAY + const.ONE_HOUR + const.ONE_MINUTE + 1
        )
        == "1 week, 1 day, 1 hour, 1 minute, 1 second"
    )


def test_pretty_type_name():
    """Test if type names are prettified correctly."""
    assert pretty_type_name("str") == "string"
    assert pretty_type_name(str) == "string"
    assert pretty_type_name("Record") == "Record"
    assert pretty_type_name(Record) == "Record"


def test_timestamp():
    """Test if timestamps are generated correctly."""
    date_time = datetime.strptime(
        "2020-01-01T00:00:00.000Z", "%Y-%m-%dT%H:%M:%S.%fZ"
    ).replace(tzinfo=timezone.utc)

    assert timestamp(date_time=date_time) == "20200101000000"
    assert timestamp(date_time=date_time, include_micro=True) == "20200101000000000000"
