# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO
from urllib.parse import urlparse

from flask import current_app

from kadi.lib.web import download_bytes
from kadi.lib.web import download_string
from kadi.lib.web import get_locale
from kadi.lib.web import get_next_url
from kadi.lib.web import make_next_url
from kadi.lib.web import static_url
from kadi.lib.web import url_for


def test_url_for(api_client, client, dummy_access_token):
    """Test if URLs are generated correctly."""
    base_url = (
        f"{current_app.config['PREFERRED_URL_SCHEME']}://"
        f"{current_app.config['SERVER_NAME']}"
    )

    # Test URL generation without request context.
    assert url_for("main.index") == base_url + "/"
    assert url_for("api.index") == base_url + "/api"

    # Test URL generation with request context.
    with client:
        client.get(url_for("main.index"))
        assert url_for("main.index") == "/"

    with api_client(dummy_access_token) as _api_client:
        _api_client.get(url_for("api.index"))
        assert url_for("api.index") == base_url + "/api"

        _api_client.get(url_for("api.index_v1_0"))

        assert url_for("api.index") == base_url + "/api/v1.0"
        assert url_for("api.index", _ignore_version=True) == base_url + "/api"


def test_static_url(monkeypatch):
    """Test if static URLs are generated correctly."""
    base_url = (
        f"{current_app.config['PREFERRED_URL_SCHEME']}://"
        f"{current_app.config['SERVER_NAME']}"
    )

    assert static_url("test") == base_url + "/static/test"

    monkeypatch.setitem(current_app.config, "MANIFEST_MAPPING", {"foo": "bar"})

    assert static_url("test") == base_url + "/static/test"
    assert static_url("foo") == base_url + "/static/bar"


def test_download_bytes():
    """Test if downloading bytes works correctly."""
    data = b"test"
    response = download_bytes(BytesIO(data), filename="test.dat")
    response.direct_passthrough = False

    assert response.status_code == 200
    assert response.data == data
    assert response.mimetype == "application/octet-stream"
    assert "Content-Disposition" in response.headers


def test_download_string():
    """Test if downloading strings works correctly."""
    data = "test"
    response = download_string(data, filename="test.txt")
    response.direct_passthrough = False

    assert response.status_code == 200
    assert response.data.decode() == data
    assert response.mimetype == "text/plain"
    assert "Content-Disposition" in response.headers


def test_get_locale(client):
    """Test if determining the locale works correctly."""
    new_locale = "de"
    default_locale = current_app.config["LOCALE_DEFAULT"]

    with client:
        # Outside a request context.
        assert get_locale() == default_locale

        client.get(url_for("main.index"))
        assert get_locale() == default_locale

        # Using the request argument.
        client.get(url_for("main.index", locale=new_locale))
        assert get_locale() == new_locale

        # Using the cookie.
        client.get(url_for("main.index"))
        assert get_locale() == new_locale

        # Using the default locale as fallback for an invalid value.
        client.get(url_for("main.index", locale="invalid"))
        assert get_locale() == default_locale


def test_next_url(client):
    """Test if creating and validating the "next" URL works correctly."""
    fallback_url = url_for("main.index")
    valid_url = url_for("records.records")
    invalid_url = "http://example.com"

    assert make_next_url(valid_url) == url_for(
        "accounts.login", next=urlparse(valid_url).path
    )
    assert make_next_url(invalid_url) == url_for("accounts.login", next=invalid_url)

    with client:
        # Outside a request context.
        assert get_next_url() == fallback_url

        client.get(url_for("main.index", next=valid_url))
        assert get_next_url() == valid_url

        client.get(url_for("main.index", next=urlparse(valid_url).path))
        assert get_next_url() == urlparse(valid_url).path

        client.get(url_for("main.index", next=invalid_url))
        assert get_next_url() == urlparse(fallback_url).path
