# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.utils import get_class_by_name
from kadi.lib.utils import named_tuple
from kadi.lib.utils import rgetattr
from kadi.lib.utils import SimpleReprMixin
from kadi.modules.records.models import Record


def test_simple_repr_mixin():
    """Test if the "SimpleReprMixin" works correctly."""

    class _Test(SimpleReprMixin):
        # pylint: disable=missing-class-docstring
        class Meta:
            representation = ["a", "b"]

        a = 1
        b = 2

    assert repr(_Test()) == "_Test(a=1, b=2)"


def test_named_tuple():
    """Test if the "named_tuple" shortcut works correctly."""
    test_tuple = named_tuple("Test", a=1, b=2)

    assert test_tuple.a == 1
    assert test_tuple.b == 2


def test_rgetattr():
    """Test if recursively getting an attribute works correctly."""
    test_obj = named_tuple("Test", a=named_tuple("Test2", b="test"))

    assert rgetattr(test_obj, "a.b") == "test"
    assert rgetattr(test_obj, "a.b.c", "default") == "default"


def test_get_class_by_name():
    """Test if getting a class by its name works correctly."""
    assert get_class_by_name("kadi.modules.records.models.Record") == Record
    assert get_class_by_name("kadi.invalid.module") is None
